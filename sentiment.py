from transformers import pipeline


classifier = pipeline('sentiment-analysis')

# Example one - to analyse in class

reviews = ["if someone taking this course has no real life experience with data modeling it is a bit difficult to understand some of the questions as they relate to the quotbusiness problemquot and the solutionbrare the questions supposed to be coming from the person initiating the data collection or from the person analyzing the databrbrotherwise interesting", "the experience was really good and easy to follow i really like the length of the videos which makes the content easy to digest pause and resume at our own pace bri was very happy to see see a takeaway document at the end of the module with the framework diagram and main points but it was not clear why quothypothesesquot was the only topic that got a little bit more attention on the page it got me wanting more", "i needed to argue on some of the things and discuss them with the instructor to get a clear idea about them br the way that the material is introduced is more theoretical and having the knowledge itself is not enoughbr i didnt like the way the module is going on limiting the ability to think and go with what is provided br the content is not engaging"]


sentiments = [classifier(sentiment) for sentiment in reviews]

# print the sentiments here - use google colab

# besides sentiment, we can do NER (Name entity recognition)

entity = pipeline("ner") # select the NLP task
sequence = "The Prague University of Economics and Business (VŠE), founded in 1953 is the biggest public university in the field of economics and business in the Czech Republic."
# print(entity(sequence)) uncomment this 



# little work in class
# uncomment and complete the following lines:

#your_review = [""]
# sentiments = use list comprehension like above and print the sentiment associated to your own review

# print the sentiment predicted by the model